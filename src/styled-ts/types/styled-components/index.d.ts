import "styled-components";
import { DefaultTheme } from "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    borderRadius: string;
    colors: {
      purple: string;
      green: string;
      yellow: string;
      lightBlue: string;
      lightRed: string;
      black: string;
      white: string;
    };
    padding: {
      bigside: string;
      normal: string;
      little: string;
    };
  }
}
