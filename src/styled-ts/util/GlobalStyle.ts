import {
  createGlobalStyle,
  DefaultTheme,
  GlobalStyleComponent
} from "styled-components";
// GlobalStyleComponent<DefaultTheme, DefaultTheme>
// @font-face {
//   font-family: 'radnika_next' ;
//   src: url('../static/radnikanext-medium-webfont.woff2') format('woff2');
//   font-weight: normal;
//   font-style: normal;
// }
export const GlobalStyle = createGlobalStyle<
  GlobalStyleComponent<DefaultTheme, DefaultTheme>
>`

  *,
  *::after,
  *::before {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
  }


  html {
   
    font-size: 62.5%; //10px

    @media (min-width: 1199.98px) {
      font-size: 75%;
    }
    
    @media (max-width: 991.98px) {
      font-size: 56.25%;
    }
    
    @media (max-width: 767.98px) {
      font-size: 50%;
    }

    @media (max-width: 575.98px) {
      font-size: 50%;
    }
  }
  
  body {
    font-family: 'Roboto', sans-serif;
    box-sizing: border-box;
    padding: 0;
    margin: 0;
    font-family: 'radnika_next';
    & #root {
      background-color: ${props => props.theme.colors.white};
      width: 1600px;
      margin: 0 auto;
      height: 100vh;
      max-height: 100vh;
    }

  }
  @media screen and (max-width: 1600px) {
    body {
      margin: 0 auto;
      & #root {
        margin: 0 auto;
        width: 98.5vw;
        height: 100vh;
        max-height: 100vh;
      }
    }
  }

  a {
    text-decoration: none;
    color: ${props => props.theme.colors.black};
  }
`;
