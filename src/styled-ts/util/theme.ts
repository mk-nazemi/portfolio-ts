//
import { DefaultTheme } from "styled-components";

export const theme: DefaultTheme = {
  borderRadius: "1rem",
  colors: {
    // purple: "#8e8bff",
    purple: "#364f6b",
    // green: "#00e6d2",
    green: "#52de97",
    yellow: "#ffe500",
    // lightBlue: "#e9eaff",
    lightBlue: "#e3fdfd",
    // lightRed: "#fbccd2",
    lightRed: "#3d84a8",
    black: "#1b262c",
    white: "#fcffff"
  },
  padding: {
    bigside: "1rem 3rem",
    normal: "0.6rem 1.6rem",
    little: "0.3rem 1rem"
  }
};
