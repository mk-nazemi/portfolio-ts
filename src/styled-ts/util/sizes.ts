interface sizePattern {
  x1?: string;
  x2?: string;
  x3?: string;
  x4?: string;
  x5?: string;
}

const sized: sizePattern = {
  x1: "575.98px",
  x2: "767.98px",
  x3: "991.98px",
  x4: "1199.98px",
  x5: "1600.1px"
};

// ============================================

export const sizes = {
  up(size: keyof sizePattern) {
    return `@media (min-width: ${sized[size]})`;
  },
  down(size: keyof sizePattern) {
    return `@media (max-width: ${sized[size]})`;
  }
};
