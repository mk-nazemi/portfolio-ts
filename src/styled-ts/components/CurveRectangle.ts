import styled from "styled-components";

interface StyledProps {
  background?: string;
  borderRadius?: string;
  width?: string;
  height?: string;
  centerIt?: boolean;
}

export const CurveRectangle = styled.div<StyledProps>`
  background-color: ${({ background }) => background || "transparent"};
  border-radius: ${({ borderRadius }) => borderRadius || "4px"};
  min-height: ${({ height }) => height || "100%"};
  max-height: ${({ height }) => height || "100%"};
  min-width: ${({ width }) => width || "100%"};
  display: flex;
  justify-content: center;
  align-items: center;
`;
