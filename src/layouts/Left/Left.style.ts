import styled, { keyframes } from "styled-components";
import { sizes } from "./../../styled-ts";

export const glitch = keyframes`
  0% {
    transform: translate(0);
  }
  20% {
    transform: translate(-1px, 1px);
  }
  40% {
    transform: translate(-1px, -1px);
  }
  60% {
    transform: translate(1px, 1px);
  }
  80% {
    transform: translate(1px, -1px);
  }
  100% {
    transform: translate(0);
  }

`;

export const LeftDiv = styled.div`
  width: 38.5%;
  height: 98vh;
  border: 9px solid #002147;
  position: relative;
  font-family: "Merriweather Sans", sans-serif;
  display: flex;

  .pic-place {
    min-width: 31.2%;
    background-color: white;
    z-index: 10;
    position: fixed;
    overflow: hidden;
    ${sizes.down("x5")} {
      min-width: 37%;
    }

    .pic-place-in {
      width: 25vw;
      z-index: 11;
      height: 25vh;
      background-color: #002147;
      margin-left: 1.2rem;
      margin-top: 1rem;
      margin-bottom: 1rem;
      position: relative;
    }
    .name {
      letter-spacing: 8px;
      padding: 0 1.6rem;
      padding-top: 0.8rem;
      font-size: 3rem;
      color: #002147;
      font-weight: bolder;

      .glitch {
        position: relative;
        line-height: 1;
        /* transform: translate(-50%, -50%); */
        margin: 0;
        &::before,
        &::after {
          content: "Mazdak Nazemi";
          display: block;
          position: absolute;
          top: 0;
          left: 0;
          height: 100%;
          width: 100%;
          opacity: 0.8;
        }
        &::before {
          color: #36454f;
          z-index: -1;
        }
        &::after {
          color: #003366;
          z-index: -2;
        }
        &:hover::before {
          animation: ${glitch} 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94) both
            infinite;
        }
        &:hover::after {
          animation: ${glitch} 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94) reverse
            both infinite;
        }
      }
    }
  }
  .developer {
    margin-top: 1.2rem;
    color: white;
    font-size: 3rem;

    background-color: #002147;
    padding: 0.5rem 1.6rem;
  }
  .container {
    position: relative;
    margin-top: 38%;
    font-size: 3rem;
    letter-spacing: 6px;
    color: ${props => props.theme.colors.black};
    display: flex;
    flex-direction: column;
    overflow-y: scroll;
    overflow-x: hidden;
    flex-direction: column;
    &::-webkit-scrollbar {
      display: none;
    }

    .more-place {
      width: 92%;
      margin: 1rem auto;
      font-size: 1rem;
      padding-top: 15rem;
      z-index: 1;
    }
  }
`;
