import React, { Component } from "react";
import { LeftDiv } from "./Left.style";
// side navbar
// https://www.youtube.com/watch?v=wV0uXXclBAI&list=PLEXedA_rYzg-BZtbuBBa34LbpcOxuqWgF&index=50
// tamtom
// https://www.youtube.com/watch?v=w_M-nZTUvDw&list=PLEXedA_rYzg-BZtbuBBa34LbpcOxuqWgF&index=85

interface AppProps {}
interface AppState {
  software: string;
  developer: string;
}
// type HTL = HTMLDivElement | HTMLSpanElement;

class _Left extends Component<AppProps, AppState> {
  private myRef = React.createRef<HTMLDivElement>();
  private myRef2 = React.createRef<HTMLDivElement>();
  constructor(props: AppProps) {
    super(props);

    this.state = {
      software: "Software",
      developer: "Developer"
    };
  }
  componentDidMount() {
    // let el = document.getElementById("str");
    // (function animate() {
    //   str.length > 0 ? (el.innerHTML += str.shift()) : clearTimeout(running);
    //   let running = setTimeout(animate, 300);
    // })();
    let stringed = this.state.software;
    let stringed2 = this.state.developer;
    let str = stringed.split("");
    let str2 = stringed2.split("");
    // this.positionAnime(str, this.myRef);
    // this.positionAnime(str2, this.myRef2);
  }

  // positionAnime = (
  //   str3: string[] = ["h"],
  //   el: React.RefObject<HTMLDivElement>
  // ) => {
  //   let running = setTimeout(this.positionAnime, 300);
  //   str3.length > 0 ? (el.innerHTML += str3.shift()) : clearTimeout(running);
  // };

  render() {
    return (
      <LeftDiv>
        <div className="pic-place">
          <div className="pic-place-in"></div>

          <div className="name">
            <div className="glitch">Mazdak Nazemi</div>
          </div>
          <div className="developer">
            <div ref={this.myRef}>{this.state.software}</div>
            <div ref={this.myRef2}>{this.state.developer}</div>
          </div>
        </div>
        <div className="container">
          <div className="more-place"></div>
        </div>
      </LeftDiv>
    );
  }
}

export const Left = _Left;
