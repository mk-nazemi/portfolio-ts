import styled, { keyframes } from "styled-components";
import { sizes } from "./../../styled-ts/util/sizes";

export const RightDiv = styled.div`
  width: 60.5%;
  height: 98vh;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  .top {
    width: 100%;
    height: 60.5%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
  }
`;

export const SkillSet = styled.div`
  border-radius: 2rem;
  width: 100%;
  height: 38.5%;
  /* background-color: #fff700; */
  background: rgb(255, 247, 0);
  background: -moz-radial-gradient(
    circle,
    rgba(255, 247, 0, 0.91) 0%,
    rgba(240, 247, 0, 0.5164040616246498) 74%
  );
  background: -webkit-radial-gradient(
    circle,
    rgba(255, 247, 0, 0.91) 0%,
    rgba(240, 247, 0, 0.5164040616246498) 74%
  );
  background: radial-gradient(
    circle,
    rgba(255, 247, 0, 0.91) 0%,
    rgba(240, 247, 0, 0.5164040616246498) 74%
  );
  border: 1px solid rgba(255, 247, 0, 0.14);
  -webkit-box-shadow: -1px 0px 118px -56px rgba(255, 247, 0, 0.38);
  -moz-box-shadow: -1px 0px 118px -56px rgba(255, 247, 0, 0.38);
  box-shadow: -1px 0px 118px -56px rgba(255, 247, 0, 0.38);
  display: flex;
  flex-direction: row;
  font-family: Consolas, monaco, monospace;
  position: relative;
  overflow: hidden;
  z-index: 1;

  &:before {
    content: "";
    position: absolute;
    background: inherit;
    z-index: -1;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    box-shadow: inset 0 0 0 3000px rgba(155, 155, 155, 0.13);
    filter: blur(10px);
    margin: -20px;
  }
  .left {
    width: 60%;
    height: 100%;
    margin: auto;
    list-style-type: none;
    letter-spacing: 2px;
    font-weight: 600;
    font-size: 1.7rem;
    padding: 1rem 0rem 1rem 3rem;

    ${sizes.up("x5")} {
      display: flex;
      flex-direction: column;
      justify-content: center;
    }
    li {
      margin-top: 0.8rem;
    }
    .html-css {
      width: 58%;
      display: flex;
      justify-content: space-between;
    }
  }

  .right {
    padding: 1rem 1rem 1rem 0rem;
    width: 40%;
    height: 100%;
    margin: auto;
    font-weight: 600;
    font-size: 1.7rem;
    list-style-type: none;
    letter-spacing: 1px;
    ${sizes.up("x5")} {
      margin-top: 4.2rem;
      margin-left: 2rem;
    }
    li {
      margin-top: 1rem;
      margin-right: 1rem;

      margin-bottom: 0.7rem;
    }
  }
`;

export const GOTO = styled.div`
  position: absolute;
  bottom: 2.6rem;
  right: 7rem;
  .shape {
    width: 3rem;
    cursor: pointer;
    height: 2rem;
    position: relative;
    background: #020403;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    &:hover {
      &:after {
        transform: scaleX(1.1);
        transition: transform 1s ease;
      }
      &:before {
        transform: scaleX(1.1);
        transition: transform 1s ease;
      }
      span {
        /* margin-right: 3rem; */
      }
    }
    span {
      /* margin-right: 3.6rem; */
      font-family: Consolas, monaco, monospace;
      transition: all 1s ease;

      font-size: 1.5rem;
      color: #020403;
      font-weight: 500;
      z-index: 4;
    }
    &:after {
      content: "";
      position: absolute;
      left: 0;
      bottom: 0;
      width: 0;
      height: 0;
      /* border-radius: 5px; */
      border-left: 1rem solid transparent;
      border-top: 1rem solid transparent;
      border-bottom: 1rem solid transparent;
    }
    &:before {
      content: "";
      position: absolute;
      right: -10px;
      bottom: 0;
      width: 0;
      height: 0;
      border-left: 10px solid #020403;
      border-top: 1rem solid transparent;
      border-bottom: 1rem solid transparent;
    }
  }
`;

export const QuoteContainer = styled.div`
  width: 100%;
  height: 35%;
  border: 5px solid #ff4500;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  font-family: "Merriweather Sans", sans-serif;
  color: #ff4500;
  background-image: url("photographer.jpg"); /* The image used */
  background-color: #cccccc; /* Used if the image is unavailable */
  height: 500px; /* You must set a specified height */
  background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: cover; /* Resize the background image to cover the entire container */
  p {
    margin-left: 2rem;
    font-size: 1.8rem;
    width: 90%;
  }
`;

export const SocialContainer = styled.div`
  border-radius: 2rem;
  /* background-color: #e24c00; */
  width: 100%;
  height: 63%;
  .rowed {
    display: flex;
    height: 49%;
    width: 100%;
    justify-content: space-evenly;
    align-items: center;
  }

  .line-container {
    display: flex;
    justify-content: space-between;
    width: 60%;
    margin: 0 auto;
    .line-splitter {
      width: 20%;
      margin: 0;
      height: 1px;
      background-color: #002147;
      background-color: #002147;
    }
  }
`;

export const Circled = styled.div`
  width: 7rem;
  height: 7rem;
  border-radius: 50%;
  background-color: #002147;
  cursor: pointer;
  transition: all 0.5s linear;
  &:hover {
    background-color: #00ff7f;
  }
`;
