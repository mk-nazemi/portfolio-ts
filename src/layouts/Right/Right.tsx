// style
import {
  RightDiv,
  SkillSet,
  GOTO,
  SocialContainer,
  QuoteContainer,
  Circled
} from "./Right.style";
// packages
import React, { Component } from "react";
//  tool tip for social or sign in page
//  https://www.youtube.com/watch?v=oYJNIbKbtU0&list=PLEXedA_rYzg-BZtbuBBa34LbpcOxuqWgF&index=132
class _Right extends Component {
  render() {
    return (
      <RightDiv>
        <SkillSet>
          <ul className="left">
            <li>TYPESCRIPT</li>
            <li>JAVASCRIPT</li>
            <li>C# .NET CORE</li>
            <li className="html-css">
              <span>HTML</span>
              <span>CSS</span>
            </li>
            <li>REACT.JS,TS VUE.JS</li>
            <li>DBD SQL NOSQL </li>
            <li>NODE & ...</li>
          </ul>
          <ul className="right">
            <li>Keeping up to date and </li>
            <li>
              Willingness <pre></pre> to work with emerging technologies
            </li>
          </ul>
          <GOTO>
            <div className="shape"></div>
          </GOTO>
        </SkillSet>
        <div className="top">
          <QuoteContainer>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam
              quibusdam at cumque soluta, quas repellat sapiente, perspiciatis
              assumenda nobis
            </p>
          </QuoteContainer>
          <SocialContainer>
            <div className="rowed">
              <Circled></Circled>
              <Circled></Circled>
              <Circled></Circled>
              <Circled></Circled>
            </div>
            <div className="line-container">
              <div className="line-splitter"></div>
              <div className="line-splitter"></div>
              <div className="line-splitter"></div>
            </div>

            <div className="rowed">
              <Circled></Circled>
              <Circled></Circled>
              <Circled></Circled>
              <Circled></Circled>
            </div>
          </SocialContainer>
        </div>
      </RightDiv>
    );
  }
}

export const Right = _Right;
