import styled from "styled-components";

export const MiddleDiv = styled.div`
  width: 38.5%;
  height: 98vh;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  .bottom {
    width: 100%;
    height: 38.5%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    .contact {
      width: 100%;
      height: 37.5%;
      background-color: #00ff7f;
      transition: all 0.2s linear;
      &:hover {
        width: 96%;
        height: 36%;
        margin-bottom: 0.75%;
        background-color: #004445;
        border-radius: 1rem;
      }
      border: 1px solid rgba(64, 224, 208, 0.14);
      -webkit-box-shadow: -1px 0px 118px -56px rgba(64, 224, 208, 0.38);
      -moz-box-shadow: -1px 0px 118px -56px rgba(64, 224, 208, 0.38);
      box-shadow: -1px 0px 118px -56px rgba(64, 224, 208, 0.38);
      a {
        font-size: 2.5rem;
        font-family: "Carrois Gothic SC", sans-serif;
        letter-spacing: 2px;
        height: 100%;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        color: white;
        transition: all 0.3s linear;
        &:hover {
          font-size: 2.6rem;
        }
        span {
          letter-spacing: 5px;
          margin-right: 1.5rem;
        }
      }
    }
  }
`;
