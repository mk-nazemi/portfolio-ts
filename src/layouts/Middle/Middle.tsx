import React, { Component } from "react";
import { MiddleDiv } from "./Middle.style";
import { Tablet, Modem } from "../../components";
import { NavLink } from "react-router-dom";
class _Middle extends Component {
  render() {
    return (
      <MiddleDiv>
        <Tablet />
        <div className="bottom">
          <Modem />
          <div className="contact">
            <NavLink to="/contact">
              <span>Contact</span> ME!
            </NavLink>
          </div>
        </div>
      </MiddleDiv>
    );
  }
}

export const Middle = _Middle;
