// styles
import { AppDiv } from "./App.style";
import { StyleProvider } from "./hoc";
// packages
import React, { Suspense } from "react";
import { Switch, Route } from "react-router-dom";
// reuse comps
import {
  MainPage,
  AboutPage,
  SkillPage,
  PortfolioPage,
  SocialPage,
  ContactPage,
  Page404
} from "./pages";

import { Spinner, ErrorBoundary } from "./components";

interface AppProps {}
// --------------------------------------------------------
const App: React.FC<AppProps> = props => {
  return (
    // location={props.location}
    <StyleProvider>
      <AppDiv>
        <ErrorBoundary>
          <Suspense fallback={<Spinner />}>
            <Switch>
              <Route exact path="/" component={MainPage} />
              <Route exact path="/about" component={AboutPage} />
              <Route exact path="/skills" component={SkillPage} />
              <Route exact path="/portfolio" component={PortfolioPage} />
              <Route exact path="/social" component={SocialPage} />
              <Route exact path="/contact" component={ContactPage} />
              <Route component={Page404} />
            </Switch>
          </Suspense>
        </ErrorBoundary>
      </AppDiv>
    </StyleProvider>
  );
};

export default App;
