// style
import { MainPageDiv } from "./MainPage.style";
import { CurveRectangle, theme } from "../../styled-ts";
// packages
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
// re use components
import { Left, Middle, Right } from "../../layouts";

// =============================================
class _MainPage extends Component {
  render() {
    return (
      <MainPageDiv>
        <Left />
        <div className="fibo-big">
          <Middle />
          <Right />
        </div>
      </MainPageDiv>
    );
  }
}

export const MainPage = _MainPage;
