import styled from "styled-components";

export const MainPageDiv = styled.div`
  min-width: 100%;
  min-height: 100vh;
  display: flex;
  justify-content: space-between;
  align-items: center;
  .fibo-big {
    width: 61%;
    height: 98vh;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;
