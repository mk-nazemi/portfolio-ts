export * from "./AboutPage/AboutPage";
export * from "./MainPage/MainPage";
export * from "./ContactPage/ContactPage";
export * from "./PortfolioPage/PortfolioPage";
export * from "./SkillPage/SkillPage";
export * from "./SocialPage/SocialPage";
export * from "./Page404/Page404";
