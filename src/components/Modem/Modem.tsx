import React, { Component } from "react";
import { CurveRectangle, theme } from "../../styled-ts";
import { ModemDiv } from "./Modem.style";

class _Modem extends Component {
  render() {
    return (
      <ModemDiv>
        <div className="the-link">The Link</div>
        <div className="active1"></div>
        <div className="active2"></div>
        <div className="active3"></div>
        <div></div>
        <div></div>
        <div className="active4"></div>
      </ModemDiv>
    );
  }
}

export const Modem = _Modem;
