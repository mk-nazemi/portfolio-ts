import styled, { keyframes } from "styled-components";

export const Rotate = keyframes`
  from {
    background-color: gray;
  }

  to {
    background-color: #00ff7f;
  }
`;

export const ModemDiv = styled.div`
  width: 100%;
  height: 60%;
  border-radius: 1.4rem;
  border: 4px solid #abafae;
  display: flex;
  justify-content: center;
  align-items: flex-end;
  position: relative;
  & div {
    margin: 1.8rem 1rem;
    width: 5px;
    height: 8px;
    border: none;
    border-radius: 50%;
    background-color: gray;
  }
  .the-link {
    background-color: transparent;
    position: absolute;
    top: 10%;
    left: 5%;
    border: 0px;
    width: 60%;
    font-size: 2rem;
    font-family: "Carrois Gothic SC", sans-serif;
    color: #abafae;
    letter-spacing:4px;
    word-spacing: 1rem;

  }
  .active1 {
    background-color: #00ff7f;
    animation: ${Rotate} 1s ease-in-out infinite;
  }
  }
  .active2 {
    background-color: #00ff7f;
    animation: ${Rotate} 0.8s ease-in-out infinite;
  }
  
  .active3 {
    background-color: #00ff7f;
    animation: ${Rotate} 0.3s ease-in-out infinite;
  }
  
  .active4 {
    background-color: #00ff7f;
    animation: ${Rotate} 0.05s linear infinite;
  }
`;
