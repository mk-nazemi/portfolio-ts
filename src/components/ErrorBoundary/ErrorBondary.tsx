import React, { Component, ErrorInfo } from "react";
import {
  ErrorImageContainer,
  ErrorImageOverlay,
  ErrorImageText
} from "./ErrorBoundary.style";

class _ErrorBoundary extends Component {
  state = { hasError: false };
  static getDerivedStateFromError = (error: ErrorEvent) => {
    return { hasError: true };
  };

  componentDidCatch(error: Error, info: ErrorInfo) {
    console.log(error);
  }

  render() {
    if (this.state.hasError) {
      return (
        <ErrorImageOverlay>
          <ErrorImageContainer />
          <ErrorImageText>Sorry this page is broken.</ErrorImageText>
        </ErrorImageOverlay>
      );
    }
    return this.props.children;
  }
}

export const ErrorBoundary = _ErrorBoundary;
