//
import React from "react";
import { SpinnerContainer, SpinnerOverlay } from "./Spinner.style";
//
const _Spinner = () => {
  return (
    <SpinnerOverlay>
      <SpinnerContainer />
    </SpinnerOverlay>
  );
};

export const Spinner = _Spinner;
