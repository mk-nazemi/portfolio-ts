import React, { Component } from "react";
import { ListUl, WhiteDiv, TabletContainer } from "./Tablet.index";
import { NavLink } from "react-router-dom";
class _Tablet extends Component {
  render() {
    return (
      <TabletContainer>
        <WhiteDiv>
          <ListUl>
            <NavLink to="/portfolio" className="btn-go">
              Portfolio Page
            </NavLink>
            <li className="item">
              <div>
                <div className="named">
                  Project name
                  <span>info</span>
                </div>
                <a href="/" className="named">
                  view
                </a>
                <a href="/">Source</a>
              </div>
            </li>
            <li className="item">
              <div>
                <div className="named">
                  Project name
                  <span>info</span>
                </div>
                <a href="/" className="named">
                  view
                </a>
                <a href="/">Source</a>
              </div>
            </li>
            <li className="item">
              <div>
                <div className="named">
                  Project name
                  <span>info</span>
                </div>
                <a href="/" className="named">
                  view
                </a>
                <a href="/">Source</a>
              </div>
            </li>
            <li className="item">
              <div>
                <div className="named">
                  Project name
                  <span>info</span>
                </div>
                <a href="/" className="named">
                  view
                </a>
                <a href="/">Source</a>
              </div>
            </li>
            <li className="item">
              <div>
                <div className="named">
                  Project name
                  <span>info</span>
                </div>
                <a href="/" className="named">
                  view
                </a>
                <a href="/">Source</a>
              </div>
            </li>
            <li className="item">
              <div>
                <div className="named">
                  Project name
                  <span>info</span>
                </div>
                <a href="/" className="named">
                  view
                </a>
                <a href="/">Source</a>
              </div>
            </li>
            <li className="item">
              <div>
                <div className="named">
                  Project name
                  <span>info</span>
                </div>
                <a href="/" className="named">
                  view
                </a>
                <a href="/">Source</a>
              </div>
            </li>
            <li className="item">
              <div>
                <div className="named">
                  Project name
                  <span>info</span>
                </div>
                <a href="/" className="named">
                  view
                </a>
                <a href="/">Source</a>
              </div>
            </li>
            <li className="item">
              <div>
                <div className="named">
                  Project name
                  <span>info</span>
                </div>
                <a href="/" className="named">
                  view
                </a>
                <a href="/">Source</a>
              </div>
            </li>
            <li className="item">
              <div>
                <div className="named">
                  Project name
                  <span>info</span>
                </div>
                <a href="/" className="named">
                  view
                </a>
                <a href="/">Source</a>
              </div>
            </li>
            <li className="item">
              <div>
                <div className="named">
                  Project name
                  <span>info</span>
                </div>
                <a href="/" className="named">
                  view
                </a>
                <a href="/">Source</a>
              </div>
            </li>
          </ListUl>
        </WhiteDiv>
      </TabletContainer>
    );
  }
}

export const Tablet = _Tablet;
