import styled from "styled-components";

export const TabletContainer = styled.div`
  background-color: #34414e;
  border-radius: 2rem;
  width: 100%;
  height: 60.5%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const WhiteDiv = styled.div`
  background-color: white;
  height: 90%;
  width: 95%;
  overflow-y: scroll;
  overflow-x: hidden;
  /* width */
  &::-webkit-scrollbar {
    display: none;
  }
`;

export const ListUl = styled.ul`
  width: 100%;
  min-height: 90%;
  display: flex;
  flex-direction: column;
  .btn-go {
    color: white;
    width: 45%;
    border: none;
    border-radius: 5px;
    background-color: #34414e;
    padding: 0.75rem 1rem;
    font-size: 1rem;
    margin: 0 auto 1rem;
    font-family: "Carrois Gothic SC", sans-serif;
    display: flex;
    justify-content: center;
    align-items: center;
    letter-spacing: 2px;
    transition: all 0.4s linear;
    &:hover {
      background-color: #6699cc;
    }
  }
  .item {
    height: 7.5rem;
    width: 96%;
    border-radius: 4px;
    margin: 0.35rem auto;
    border: 2px solid #abafae;
    list-style-type: none;
    display: flex;
    div {
      display: flex;
      align-items: center;
      justify-content: space-evenly;
      width: 100%;
      font-family: "Carrois Gothic SC", sans-serif;
      .named {
        cursor: pointer;
      }
      a {
        margin-right: 3rem;
        transition: all 0.4s linear;
        &:hover {
          color: #00ff7f;
          transform: scale(1.1);
        }
      }
    }
  }
  &:first-child {
    margin-top: 2rem;
  }
`;
