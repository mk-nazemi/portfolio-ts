import React from "react";
import { ThemeProvider, DefaultTheme } from "styled-components";
import { theme, GlobalStyle } from "../../styled-ts";

interface AppProps {
  theme?: DefaultTheme;
}

const _StyleProvider: React.FC<AppProps> = props => {
  return (
    <ThemeProvider theme={theme}>
      <React.Fragment>
        {props.children}
        <GlobalStyle />
      </React.Fragment>
    </ThemeProvider>
  );
};

export const StyleProvider = _StyleProvider;
